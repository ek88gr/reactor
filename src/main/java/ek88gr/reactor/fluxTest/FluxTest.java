package ek88gr.reactor.fluxTest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Slf4j
@Component
public class FluxTest {

    private int _testNumbers(int value) {
        if (value > 2) {
            System.out.println("Failed");
            throw new IllegalArgumentException("value is too high!");
        }
        return value;
    }

    //doOnEach
    public Flux<Integer> doOnEachMethod() {
        Flux.range(1, 5)
                .map(this::_testNumbers)
                .doOnEach(
                        item -> {
//                            if (item.isOnComplete()) System.out.println("Item done!");
                            if (item.isOnComplete()) System.out.println("Item done!");
                            else if (item.isOnError()) System.out.println("Item fail!");
                        }
                )
                .subscribe(System.out::println);
        return null;
    }

    //doOnError
    public Flux<Integer> checkOnErrorMethodRetry() {
        Flux.just(1, 2, 3)
                .map(this::_testNumbers)
                .doOnError(e -> {
                    // to do something
                    System.out.println("Log error");
                })
                .subscribe(value -> System.out.println("Value: " + value));
        return null;
    }

}
