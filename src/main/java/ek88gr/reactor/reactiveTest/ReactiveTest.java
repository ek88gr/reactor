package ek88gr.reactor.reactiveTest;

import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Subscription;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import reactor.core.publisher.*;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

@Slf4j
@Component
public class ReactiveTest {

    public void reactive_019() {
        AtomicLong al = new AtomicLong(-20000);
    }

    public void reactive_018() {
        List<Double> doubleList = new ArrayList<>();
        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            var randomNum = Math.random() * 100;
            doubleList.add(randomNum);
        }

        Flux<Double> doubleFlux = Flux.fromIterable(doubleList);
        AtomicInteger count = new AtomicInteger();
        var disposable = doubleFlux
                .map(d -> {
                    var str = d.toString();
                    String subStr = "";
                    if (str.contains(".")) {
                        var index = str.indexOf(".");
                        subStr = str.substring(0, index);
                    }
                    stringList.add(subStr);
                    return subStr;
                })
                .subscribeOn(Schedulers.boundedElastic())
                .doOnComplete(() -> {
                    System.out.println("doOnComplete");
                })
                .subscribe(data -> {
                    count.getAndIncrement();
                    System.out.println("count: " + count + ", data: " + data + ", thread: " + Thread.currentThread().getName());
                })
                /*.dispose()*/;

    }

    //.log()
    public void reactive_017() {
        try {
            Flux<Long> flux = Flux.interval(Duration.ofMillis(300))
                    .filter(data -> data % 2 == 0);
            flux./*log().*/subscribe(l-> System.out.println(l));
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void reactive_016() {
        var mono = Mono.just("mono test test");
        mono
                .doOnNext(System.out::println)
                .subscribe();
    }

    public void reactive_015() {
        var dateFlux = Flux.range(2000, 24);
        dateFlux
                .doOnNext(i -> {
                    System.out.println("date: " + i);
                    if (i == 2023) {
                        throw new RuntimeException("date == " + i);
                    }
                })
                .doOnError(e -> {
                    System.out.println("doOnError");
                })
                .subscribe();

    }

    //важно!
    public void reactive_014() {

//        var conn = Mono.just(Flux.just("","","",""));
//        conn.subscribe(v->{
//            v.subscribe(v1->{
//                System.out.println(v1);
//            })
//        })
//
//        var d = stringList.doOnNext(v->{}).doFinally().subscribe(v->{});
//        d.dispose();

        var flux = Flux.just("A", "B", "C", "D", "E");

        flux
                .doOnNext(System.out::println)
                .subscribe();

        flux
                .error(new IllegalArgumentException())
                .doOnError(System.out::println).subscribe();
        System.out.println("отработал");

        var d = flux
                .doOnCancel(() -> {
                    System.out.println("doOnCancel");
                }).subscribe(
                        v1 -> System.out.println("v1"),
                        v2 -> System.out.println("v2"),
                        () -> {
                        },
                        Subscription::cancel
                );

        Consumer<SignalType> consumer = signalType -> {
//            signalType = SignalType.ON_COMPLETE;
            System.out.println("hello, world");
        };

        //Flux - издатель
        flux
                .doOnNext(System.out::println)
                .doFinally(signalType -> {
                    if (signalType == SignalType.ON_COMPLETE) {
                        System.out.println("ON_COMPLETE");
                    } else if (signalType == SignalType.ON_NEXT) {
                        System.out.println("ON_NEXT");
                    }

                })
                .subscribe();

    }

    public void reactive_013() {
        System.out.println("Мы зашли в метод");
        Flux.just(1, 2, 0)
//                .map(i -> "100 / " + i + " = " + (100 / i))
                .map(integer -> {
                    System.out.println(100 / integer);
                    return integer;
                })
                .subscribe(
                        value -> System.out.println("ok"),
                        error -> System.out.println("___error---: " + error.getMessage())
                );
        /*
         * doOnNext
         * doOnCancel
         * doFinally
         * mono.create
         * */

    }

    public void reactive_012() {
//        List<String> strings = Arrays.asList("ABC", "DE", "FGHI", "JKLMNO");
        String str = "ABCxyz";
        var mono = Mono.create(ms -> {
            System.out.println(str);
        });
        mono.doOnNext(o -> {
            if (o.equals("ABCxyz")) {
                System.out.println(o);
            } else {
                System.out.println("error");
            }
        }).subscribe();

    }

    //толковый пример, обработка ошибок
    public void reactive_011() {

        List<Integer> numbers = Arrays.asList(1, 2, 0);
        Consumer<FluxSink<Integer>> fluxSinkConsumer = i -> {
            numbers.forEach(v -> System.out.println(100 / v));
        };
//        var flux = Flux.create(fluxSinkConsumer);

        //1.
//        var flux = Flux.create(err -> err.error(new Throwable("error_error-error")));
//        flux.subscribe();
//
//        System.out.println("Ошибка перехвачена");

        //2.
        Consumer<SignalType> signalTypeConsumer = signalType -> {
            if (signalType == SignalType.ON_ERROR) {
                System.out.println("SignalType.ON_ERROR");
            }
        };

        var flux = Flux.fromStream(numbers.stream());
        flux.doFinally(signalTypeConsumer).subscribe();


//        flux.subscribe(
//                value -> System.out.println("RECEIVED: " + value),
//                error -> {
//                    System.out.println("CAUGHT: " + error.getMessage());
//                }
//        );

//        List<Integer> numbers = Arrays.asList(1, 2, 0);
//        Consumer<FluxSink<Integer>> fluxSinkConsumer = i -> {
//            numbers.forEach(v -> System.out.println(100 / v));
//        };
//        var flux = Flux.create(fluxSinkConsumer);
//        flux.subscribe(
//                value -> System.out.println("RECEIVED: " + value),
//                error -> System.out.println("CAUGHT: " + error.getMessage())
//        );

    }

    //Consumer (2 варианта записи)
    public void reactive_010() {

        List<String> list = Arrays.asList("A", "B", "C", "D", "E");

        //1.
        Consumer<MonoSink<String>> monoSinkConsumer = s -> {
            list.forEach(v -> System.out.println(v));
        };
        var mono1 = Mono.create(monoSinkConsumer);
//        var disposable1 = mono1.subscribe();

        //2. короткий вариант
//        var disposable2 = Mono.create(monoSink -> {
//            list.forEach(v -> System.out.println(v));
//        }).subscribe();

        mono1
                .doOnNext(s -> System.out.println("doOnNext"))
                .doFinally(signalType -> System.out.println(SignalType.ON_NEXT))
                .doOnCancel(() -> System.out.println("doOnCancel"))
                .subscribe(s -> System.out.println("subscribe"));

        var d1 = mono1.doOnNext(v -> {
            System.out.println("DO ON NEXT" + v);
        }).subscribe();

//        mono1.doOnError(err -> {
//            System.out.println(err.getMessage());
//        }).subscribe();
//
//        mono1.doOnCancel(() -> {
//            System.out.println("DO ON CANCEL");
//        }).subscribe();
//
//        mono1.doFinally(s -> {
//            System.out.println("*************8" + s.name());
//        }).subscribe();

    }

    //Consumer Flux
    public void reactive_009() {

        Consumer<FluxSink<Integer>> consumer = integer -> {
            List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5);
            integerList.forEach(System.out::println);
        };

        var flux = Flux.create(consumer).subscribe();

    }

    //Consumer (продолжение)
    public void reactive_008() {

        //MonoSink<Integer>, а не Consumer<Integer>
        Consumer<MonoSink<Integer>> consumer = integer -> {
            List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5);
            integerList.forEach(num -> System.out.println(num));
        };
        Disposable mono = Mono.create(consumer).subscribe();

    }

    //Consumer Mono
    public void reactive_007() {

        List<String> list = Arrays.asList("A", "B", "C", "D", "E");

        //реактивный вариант
        Consumer<MonoSink<String>> consumer1 = str -> {
            list.forEach(v -> System.out.println(v));
        };
        var mono = Mono.create(consumer1).subscribe();

        //императивный вариант
        var consumer2 = new Consumer<MonoSink<String>>() {
            @Override
            public void accept(MonoSink<String> objectMonoSink) {
                list.forEach(str -> System.out.println(str));
            }
        };
        var mono2 = Mono.create(consumer2).subscribe();

    }

    public void reactive_006() {
        var pairs = Arrays.asList("usdtrub@trade", "btcrub@trade", "ethrub@trade", "busdrub@trade", "bnbrub@trade");
        Consumer<String> consumer = s -> {
        };
        Flux.fromIterable(pairs)
                .doOnNext(v -> System.out.println(v))
                .subscribe();
    }

    public void reactive_005() {
        Flux.just("A", "B", "C")
                .subscribe(
                        data -> log.info("onNext: {}", data),
                        err -> { /* игнорируется */ },
                        () -> log.info("onComplete"));
    }

    public void reactive_004() {
        Flux<Integer> ints = Flux.range(1, 4)
                .map(i -> {
                    if (i <= 3) return i;
                    throw new RuntimeException("Go to 4");
                });
        ints.subscribe(
                System.out::println,
                error -> System.out.println("Error: " + error)
        );
    }

    public void reactive_003() {
        Flux<Integer> ints = Flux.range(1, 3);
//        ints.subscribe(integer -> System.out.println(integer));
        ints.subscribe(System.out::println);
    }

    public void reactive_002() {
        Flux<String> sequence1 = Flux.just("foo", "bar", "foobar");
        List<String> iterable = Arrays.asList("foo", "bar", "foobar");
        Flux<String> sequence2 = Flux.fromIterable(iterable);
//        sequence2.subscribe(s -> System.out.println(s));
        sequence2.subscribe(System.out::println);
    }

    public void reactive_001() {
        Flux<String> stream01 = Flux.just("A", "B", "C");
        stream01
                .doOnNext(s -> {
                    System.out.println("stream01");
                })
                .subscribe();
    }

}
