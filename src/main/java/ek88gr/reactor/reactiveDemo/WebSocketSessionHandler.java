package ek88gr.reactor.reactiveDemo;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.util.concurrent.Queues;

import java.time.Instant;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Getter
public class WebSocketSessionHandler implements WebSocketHandler {
    private final Sinks.Many<String> sink = Sinks.many().multicast().onBackpressureBuffer(Queues.SMALL_BUFFER_SIZE, false);
    private final Flux<String> STREAM;
    private Flux<String> flux;

    //частота проверки задания (как часто высчитывать timeDifference)
    private final Long TIMER_PERIOD = 1 * 1000L;
    //время, в течение которого не приходят данные с сервера
    private final Long DOWNTIME = 10 * 1000L;

    AtomicLong LAST_DATA_TIME = new AtomicLong(-1L);
    Timer TIMER;

    public WebSocketSessionHandler() {
        this.STREAM = sink.asFlux().retry();
    }

    public Flux<String> stream() {
        return STREAM;
    }

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        this.flux = session
                .receive()
                .map(WebSocketMessage::getPayloadAsText)
                .doOnNext(v -> {
                    this.LAST_DATA_TIME.set(Instant.now().toEpochMilli());
                    System.out.println(v);
                    sink.tryEmitNext(v);
                });

        return this.flux.then();
    }

    public void timeCounter() {
        if (LAST_DATA_TIME.get() == -1) return;
        LAST_DATA_TIME.updateAndGet((v) -> Instant.now().toEpochMilli());
        if (TIMER == null) {
            TIMER = new Timer();
        }
        TIMER.scheduleAtFixedRate(new Tt(LAST_DATA_TIME, sink) {
            @Override
            public void run() {
                super.run();
            }
        }, 0, TIMER_PERIOD);
    }

    private class Tt extends TimerTask {
        final AtomicLong LAST_DATA_TIME;
        private final Sinks.Many<String> sink;

        public Tt(AtomicLong LAST_DATA_TIME, Sinks.Many<String> sink) {
            this.sink = sink;
            this.LAST_DATA_TIME = LAST_DATA_TIME;
        }

        @Override
        public void run() {
            long timeDifference = Instant.now().toEpochMilli() - LAST_DATA_TIME.get();
            if (timeDifference > DOWNTIME) {
                //todo: reconnect
                this.sink.tryEmitError(new Throwable("Stopped receiving data from the stream. Reconnecting..."));
                log.error("Stopped receiving data from the stream. Reconnecting...");
                log.info("Thread_name: {}, timeDifference: {}", Thread.currentThread().getName(), timeDifference);
                System.out.println("");
            }
        }
    }

}