package ek88gr.reactor.reactiveDemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import reactor.util.concurrent.Queues;

import java.net.URI;
import java.time.Instant;

@Slf4j
public class Wrapper {
    private final WebSocketSessionHandler handler = new WebSocketSessionHandler();

    private final Sinks.Many<String> sink = Sinks.many().multicast().onBackpressureBuffer(Queues.SMALL_BUFFER_SIZE, false);
    private final Flux<String> flux;

    private final WebSocketClient client = new ReactorNettyWebSocketClient();
    private final URI CONNECTION_URI;

    public Wrapper(String url) {
        CONNECTION_URI = URI.create(url);
        this.flux = sink
                .asFlux()
                .share();
    }

    public Flux<String> connect() {
        var flux = _execute();
        handler.getLAST_DATA_TIME().set(Instant.now().toEpochMilli());
        handler.timeCounter();
        return this.flux;
    }

    private Flux<String> _execute() {
        var connection = client
                .execute(CONNECTION_URI, this.handler)
                .doOnError(e -> {
                    log.error("Server connection error. Reconnection...");
                })
                .retry()
                .subscribe();
        return this.handler.stream();
    }
}