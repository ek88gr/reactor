package ek88gr.reactor.reactiveDemo;

import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Subscription;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Component
public class ReactiveDemo {

    public String _getUrl(List channels) {
        StringBuilder url = new StringBuilder("wss://stream.binance.com:9443/stream?streams=");
        url.append(channels.get(0));

        for (int i = 1; i < channels.size(); i++) {
            url.append("/").append(channels.get(i));
        }

        return url.toString();
    }

    public void getFluxFromIntegerList() {
        var numberListFlux = Flux.just(1, 2, 5, 10, 0);
        AtomicInteger count = new AtomicInteger();
        numberListFlux
                .doOnNext(integer -> {//1.
                    count.getAndIncrement();
                    System.out.println("doOnNex worked. 100 / value = " + 100 / integer + ", count: " + count);
                })
                .doOnError(e -> {//2.
                    System.out.println("doOnError worked");
                })
                .doOnCancel(() -> {
                    System.out.println("doOnCancel worked");
                })
                .doFinally(s -> {//3.
                    System.out.println("doFinally worked");
                })
                .subscribe(
//                        System.out::println,
//                        e -> {},
//                        () -> {},
//                        subscription -> subscription.cancel()
                );
        System.out.println("All code is worked!\n");
    }

    public void getMonoFromInteger() {

        var numberMono = Mono.just(13);
        numberMono
                .doOnNext(integer -> {
                    System.out.println("doOnNext");
                })
                .doOnError(throwable -> {
                    System.out.println("doOnError");
                })
                .doOnCancel(() -> {
                    System.out.println("doOnCancel");
                })
                .doFinally(signalType -> {
                    System.out.println("doFinally");
                })
                .subscribe();

    }

}
