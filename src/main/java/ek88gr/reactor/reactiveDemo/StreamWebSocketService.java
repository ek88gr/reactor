//package ek88gr.reactor.reactiveDemo;
//
//import org.springframework.stereotype.Service;
//import reactor.core.publisher.Flux;
//import reactor.core.publisher.Sinks;
//import reactor.util.concurrent.Queues;
//
//import java.util.HashMap;
//import java.util.Optional;
//
//@Service
//public class StreamWebSocketService {
//
//    private final Sinks.Many<String> sink = Sinks.many().multicast().onBackpressureBuffer(Queues.SMALL_BUFFER_SIZE, false);
//    private final Flux<String> flux;
//
//    private HashMap<String, Sinks.Many<String>> WRAPPER_MAP = new HashMap<>();
//    private HashMap<String, Wrapper> WRAPPER_MAP = new HashMap<>();
//
//    public Flux<MarkPriceStream> markPriceStream(String key){
//
//       var wrapper =  Optional.ofNullable(WRAPPER_MAP.get(key)).orElseGet(v->{
//            var wrapper = new Wrapper();
//            this.WRAPPER_MAP.put(key, wrapper);
//             return wrapper;
//        });
//
//       return wrapper.join().map(v->{
//           return new Object();
//       }).subscribe();
//    }
//
//}