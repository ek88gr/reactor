package ek88gr.reactor.binance;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
@Component
public class WebSocketClientBinance {
    private static final String BASE_SOCKET_URL = "wss://stream.binance.com:9443";


    public Mono<Void> stream(List channels, Consumer<String> onNext) {
        WebSocketClient client = new ReactorNettyWebSocketClient();

        StringBuilder url = new StringBuilder(BASE_SOCKET_URL + "/stream?streams=");
        url.append(channels.get(0));

        for (int i = 1; i < channels.size(); i++) {
            url.append("/").append(channels.get(i));
        }

        final URI uri = URI.create(url.toString());

        return client.execute(uri, session -> session.receive()
                .map(WebSocketMessage::getPayloadAsText)
                .doOnNext(onNext)
                .then());
    }

}
