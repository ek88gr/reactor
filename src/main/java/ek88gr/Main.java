package ek88gr;

import ek88gr.reactor.binance.WebSocketClientBinance;
import ek88gr.reactor.fluxTest.FluxTest;
import ek88gr.reactor.reactiveDemo.ReactiveDemo;
import ek88gr.reactor.reactiveDemo.Wrapper;
import ek88gr.reactor.reactiveTest.ReactiveTest;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Autowired
    ReactiveTest reactiveTest;

    @Autowired
    FluxTest fluxTest;

    @Autowired
    ReactiveDemo reactiveDemo;

    @Autowired
    WebSocketClientBinance webSocketClientBinance;

    @PostConstruct
    void runner() {

//        _getDataPairV1();
//        _getDataPairV2();
//        _getDataPairV3();
//        _getDataPairV4();
//        _getDataPairV5();
//        _getDataPairV6();

        wrapperRun();

//        reactiveDemo.getFluxFromIntegerList();
//        reactiveDemo.getMonoFromInteger();
//        System.out.println(fluxTest.doOnEachMethod());
//        System.out.println(fluxTest.checkOnErrorMethodRetry());
//        reactiveTest.reactive_018();
//        reactiveTest.reactive_017();
//        reactiveTest.reactive_016();
//        reactiveTest.reactive_015();
//        reactiveTest.reactive_014();
//        reactiveTest.reactive_013();
//        reactiveTest.reactive_012();
//        reactiveTest.reactive_011();
//        reactiveTest.reactive_010();
//        reactiveTest.reactive_009();
//        reactiveTest.reactive_008();
//        reactiveTest.reactive_007();
//        reactiveTest.reactive_006();
//        reactiveTest.reactive_005();
//        reactiveTest.reactive_004();
//        reactiveTest.reactive_003();
//        reactiveTest.reactive_002();
//        reactiveTest.reactive_001();

    }

    void wrapperRun() {
        Wrapper wrapper = new Wrapper(reactiveDemo._getUrl(Arrays.asList("usdtrub@trade", "btcrub@trade", "ethrub@trade")));
        wrapper.connect().subscribe();
    }

    void _getDataPairV6() {
        var monoConnection = _connect();
        var disposable = monoConnection
                .publishOn(Schedulers.boundedElastic())
                .doOnError(e -> {
                    log.error("Fail connection to server", e);
                    monoConnection.retry(Long.MAX_VALUE).subscribe();
                    System.out.println("RECONNECT");
                })
                .subscribe();

        Flux
                .interval(Duration.ofMillis(20_000))
                .publishOn(Schedulers.boundedElastic())
                .doOnNext(v -> {
                    System.out.println("закрытие потока");
                    disposable.dispose();
                    System.out.println("повторное подключение, открытие потока...");
                    monoConnection.retry(Long.MAX_VALUE).subscribe();
                })
                .subscribe();
    }

    //work variant
    void _getDataPairV5() {
        var monoConnection = _connect();
        var disposable = monoConnection
                .publishOn(Schedulers.boundedElastic())
                .doOnError(e -> {
                    log.error("Fail connection to server", e);
                    var d = monoConnection.retry(Long.MAX_VALUE).subscribe();
                    System.out.println("RECONNECT");
                })
                .subscribe();

        Flux
                .interval(Duration.ofMillis(60_000))
                .publishOn(Schedulers.boundedElastic())
                .doOnNext(v -> {
                    System.out.println("закрытие потока");
                    disposable.dispose();
                    System.out.println("повторное подключение, открытие потока...");
                    monoConnection.retry(Long.MAX_VALUE).subscribe();
                })
                .subscribe();
    }

    void _getDataPairV4() {

        var monoConnection = _connect();
        var disposable = monoConnection
                .publishOn(Schedulers.boundedElastic())
                .doOnError(e -> {
                    log.error("Fail connection to server", e);
                    monoConnection.retry(Long.MAX_VALUE).subscribe();
                    System.out.println("RECONNECT");
                })
//                .timeout(Duration.ofSeconds(1_000))
                .take(Duration.ofMillis(10_000))
                .doOnNext(s -> {
                    System.out.println("закрытие потока. реконнект");
                    _connect();
                })
                .subscribe();
    }

    void _getDataPairV3() {

//        var duration = Duration.ofMillis(60_000);
//        var intervalFlux = Flux.interval(duration);
//
//        Disposable disposable = intervalFlux.subscribe();
//        if (duration == ) {
//            disposable.dispose();
//        }

        var monoConnection = _connect();
        monoConnection
                .publishOn(Schedulers.boundedElastic())
                .doOnError(e -> {
                    log.error("Fail connection to server", e);
                    monoConnection.retry(Long.MAX_VALUE).subscribe();
                    System.out.println("RECONNECT");
                })
                .retry(Long.MAX_VALUE)
                .subscribe();
    }

    void _getDataPairV2() {
        var d = "";
        var monoConnection = _connect();
        monoConnection
                .publishOn(Schedulers.boundedElastic())
//                //это то, что нужно или нужно обрабатывать ошибку внутри subscribe
                //см. ниже, результат одинаковый (или снизу вывести ошибку, а сверху переподключение...)
                .doOnError(e -> {
                    //1.
                    monoConnection.retry(Long.MAX_VALUE).subscribe();
                    System.out.println("RECONNECT");
                })
                .doOnCancel(() -> {
                    System.out.println("Как зайти в этот блок кода?");
                })
                //3.
                .doFinally(s -> {
                    System.out.println("doFinally worked. signalType: " + s);
                })
                .subscribe(
                        s -> {
                        },
                        e -> {
                            //2.
                            monoConnection.retry(Long.MAX_VALUE).subscribe();
//                            throw new RuntimeException("СООБЩЕНИЕ: " + e.getMessage());
                        },
                        () -> {
                        }/*,
                        subscription -> subscription.request(Long.MAX_VALUE)*/
                );
    }

    void _getDataPairV1() {

//        Flux.interval()

        var dataMono = _connect();

        var d = dataMono
                .doOnNext(s -> {
                    System.out.println("doOnNext" + s);
                })
                .publishOn(Schedulers.boundedElastic())
                .doOnError(e -> {
                    dataMono.retry(Long.MAX_VALUE).subscribe();
                    System.out.println("RECONNECT");
                })
                .doOnCancel(() -> {
                    System.out.println("Как зайти в этот блок кода?");
                })
                .doFinally(s -> {//signalType?
                    System.out.println("doFinally worked. signalType: " + s);
                })
                .subscribe(
//                        System.out::println,
//                        e -> {},
//                        () -> {},
//                        subscription -> subscription.cancel()
                );


        d.dispose();
    }

    Mono<Void> _connect() {
        AtomicInteger count = new AtomicInteger();
        return webSocketClientBinance.stream(
                Arrays.asList("usdtrub@trade", "btcrub@trade", "ethrub@trade"),
                s -> {
                    count.getAndIncrement();
                    if (count.get() % 10 == 0) {
                        System.out.println(count);
                    }
                    System.out.println(s);
                }
        );
    }

}
