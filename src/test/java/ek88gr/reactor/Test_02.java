package ek88gr.reactor;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.stream.Stream;

//@Slf4j
@SpringBootTest
public class Test_02 {

    @Test
    public void createAFlux_fromStream() {
        Stream<String> fruitStream =
                Stream.of("Grape", "Grape", "Grape", "Grape", "Grape");
        Flux<String> fruitFlux = Flux.fromStream(fruitStream);
        StepVerifier.create(fruitFlux)
                .expectNext("Grape")
                .expectNext("Grape")
                .expectNext("Grape")
                .expectNext("Grape")
                .expectNext("Grape")
                .verifyComplete();
    }

}
